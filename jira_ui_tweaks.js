// ==UserScript==
// @name          JIRA UI Tweaks
// @namespace     http://agileadam.com
// @version       0.3
// @description   Basic JIRA tweaks. Add "Status" field to your backlog card layout and see colored status labels. bitbucket.org/agileadam/jira-ui-tweaks
// @match         https://*.atlassian.net/secure/RapidBoard.jspa*
// ==/UserScript==

// TODO consider adding these styles into this script. Set these using Stylebot for now.
// .ghx-extra-field { padding: 0 6px; }
// .ghx-extra-field-seperator { display: none; }
// .ghx-extra-field:first-child { margin-right: 10px; text-transform: uppercase; }
// .ghx-issue-compact .ghx-issue-content { display: flex; font-family: Arial; font-size: 12px; }
// .ghx-plan-band-2 .ghx-issue-compact .ghx-end.ghx-row { position: absolute; right: 0; top: 2px; margin: 0; }
// .ghx-plan-band-1 .ghx-issue-compact .ghx-end.ghx-row { position: absolute; right: 0; top: 2px; margin: 0; }
// .ghx-plan-band-1 .ghx-issue-compact .ghx-end.ghx-row .ghx-end { position: relative; }
// .ghx-plan-extra-fields { color: #000; font-family: Futura; font-size: 11px; height: 16px; line-height: 180%; margin-left: 10px; margin-top: 8px; }
// .ghx-summary { font-family: Arial; font-size: 12px; margin-top: 3px; }

AJS.$(document).ready(function() {
    function refreshStatusStyles() {
        console.log('iFactory JIRA Tweaks: Adding status colors');

        // Status names shown here: https://YOURDOMAIN.atlassian.net/secure/admin/ViewStatuses.jspa
        // All items seem to get rebuilt when any one thing changes, so no need to check for processed before updating each item.

        // Blocked
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Needs Information"]').addClass('ifjt-processed').css('background-color', 'rgb(255,182,86)').text('Needs Info');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Client Feedback"]').addClass('ifjt-processed').css('background-color', 'rgb(255,182,86)');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: BLOCKED"]').addClass('ifjt-processed').css('background-color', 'rgb(255,182,86)');

        // To Do
        AJS.$('span.ghx-extra-field[data-tooltip="Status: To Do"]').addClass('ifjt-processed').css('background-color', 'rgb(255,254,219)');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Not Approved"]').addClass('ifjt-processed').css('background-color', 'rgb(244,66,66)');

        // In Progress
        AJS.$('span.ghx-extra-field[data-tooltip="Status: In Progress"]').addClass('ifjt-processed').css('background-color', 'rgb(255,251,137)');

        // Approve/Review
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Technical Review"]').addClass('ifjt-processed').css('background-color', 'rgb(204,204,0)').text('Tech Review');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Quality Review"]').addClass('ifjt-processed').css('background-color', 'rgb(204,204,0)');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Client Approval"]').addClass('ifjt-processed').css('background-color', 'rgb(204,204,0)');

        // Approved
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Approved"]').addClass('ifjt-processed').css('background-color', 'rgb(206,255,137)');

        // Staged
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Staged"]').addClass('ifjt-processed').css('background-color', 'rgb(42,249,77)');

        // Done
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Done"]').addClass('ifjt-processed').css('background-color', 'rgb(13,226,49)').parents('.ghx-issue-content').find('.ghx-summary').css('text-decoration', 'line-through');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Launched"]').addClass('ifjt-processed').css('background-color', 'rgb(13,226,49)').parents('.ghx-issue-content').find('.ghx-summary').css('text-decoration', 'line-through');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Rejected"]').addClass('ifjt-processed').css('background-color', 'rgb(13,226,49)').parents('.ghx-issue-content').find('.ghx-summary').css('text-decoration', 'line-through');
        AJS.$('span.ghx-extra-field[data-tooltip="Status: Duplicate"]').addClass('ifjt-processed').css('background-color', 'rgb(13,226,49)').parents('.ghx-issue-content').find('.ghx-summary').css('text-decoration', 'line-through');

        // Other misc. changes (TODO probably should move into separate function or rename this function)
        AJS.$('span[data-tooltip="Σ Remaining Estimate: None"]').addClass('ifjt-processed').css('color', 'red');
    }

    function createIssueDialogCleanup() {
        console.log('iFactory JIRA Tweaks: cleaning up dialog');
		// Create Issue dialog
        AJS.$('#qf-field-timetracking .description').remove();
        AJS.$('#qf-field-labels .description').remove();
        AJS.$('.jira-wikifield .field-tools').remove();
        AJS.$('#qf-field-fixVersions .description').remove();
        AJS.$('#qf-field-issuelinks .description').remove();
        AJS.$('#qf-field-customfield_10004 .description').remove();
        AJS.$('#qf-field-customfield_10008 .description').remove();
        AJS.$('textarea#description').attr('rows', 3);
        AJS.$('.qf-field-issuetype .aui-iconfont-help').remove();
        AJS.$('.qf-field-issuetype').css('float', 'right').css('margin-top', '-47px').css('padding', '0px');
        AJS.$('.qf-field-issuetype .description').remove();

		// Edit Issue dialog
        AJS.$('.description:contains("Start typing to")').remove();
        AJS.$('.description:contains("Begin typing to")').remove();
        AJS.$('.description:contains("Choose an epic to assign")').remove();
        AJS.$('.description:contains("JIRA Software sprint field")').remove();
        AJS.$('.description:contains("estimate of how much work")').remove();
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
        var $context = AJS.$(context);
        // This was not reliable, so we're going to keep looping (see runEveryXSecs())
        // but only do the heavy lifting if we've lost the styles.
        //if (reason == JIRA.CONTENT_ADDED_REASON.panelRefreshed) {
        //    refreshStatusStyles();
        //}
        if (reason == JIRA.CONTENT_ADDED_REASON.dialogReady) {
            if ($context[0].innerHTML.indexOf('Issue Type') !== -1) {
                createIssueDialogCleanup();
            }
        }
    });

    function runEveryXSecs() {
        // Only do the heavy lifting if there aren't any processed items
        if ($('.ifjt-processed').length === 0 && AJS.$('#ghx-backlog').length && AJS.$('.js-issue').length) {
            refreshStatusStyles();
        }
    }

    var runEveryXSecsInterval = setInterval(runEveryXSecs, 250);
});