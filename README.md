# README #

I created this to accommodate some personal preferences with respect to the Jira user interface.

This includes:

- Colored status labels for all iFactory status types
- Cleanup the Create Issue dialog
- Cleanup the Edit Issue dialog

See note at top of jira_ui_tweaks.js re: styling. I'm currently using Stylebot to make things look even better (tighter, cleaner, etc.)